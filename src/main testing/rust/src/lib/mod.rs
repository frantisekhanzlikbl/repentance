#![feature(array_value_iter)]
#![feature(type_ascription)]
#![feature(min_specialization)]
#![deny(unsafe_code)]
// warnings are ok during development, but have to be explicitly allowed at usage site if required during release
#![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
#![warn(clippy::cargo)]
#![allow(clippy::cargo_common_metadata)]
// this is triggered by external dependencies and can't be fixed by us
#![allow(clippy::multiple_crate_versions)]

#[allow(unused_imports)]
#[macro_use]
extern crate log;

mod components;
mod initialization;

use components::App;
use repentance_runtime::{mountable::Mountable, node_factories::Context};
use std::mem::forget;
use wasm_bindgen::prelude::wasm_bindgen;
use web_sys::Node;

#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen(start)]
pub fn main() {
	initialization::init();

	let window = web_sys::window().unwrap();
	let document = window.document().unwrap();
	let root: Node = document
		.get_element_by_id("root")
		.expect("the document should contain an element with id `root`")
		.into();

	let app = App::new(&Context {
		document: &document,
	});

	app.mount(&root);

	forget(app);
}
