#![feature(min_const_generics)]
#![feature(min_specialization)]
#![deny(unsafe_code)]
// warnings are ok during development, but have to be explicitly allowed at usage site if required during release
#![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
#![warn(clippy::cargo)]
#![allow(clippy::cargo_common_metadata)]
// this is triggered by external dependencies and can't be fixed by us
#![allow(clippy::multiple_crate_versions)]

pub mod mountable;
pub mod node_factories;

pub use mountable::Mountable;
