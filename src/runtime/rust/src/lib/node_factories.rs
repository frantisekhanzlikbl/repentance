use crate::mountable::{impls::NoOpMountable, Mountable};
use js_sys::Function;
use wasm_bindgen::JsCast;
use web_sys::{Document, Element, HtmlInputElement, Text};

pub struct Context<'a> {
	pub document: &'a Document,
}

pub fn element(c: &Context, name: &str, children: &(impl Mountable + ?Sized)) -> Element {
	let e = c.document.create_element(name).unwrap();

	children.mount(&e);

	e
}

#[must_use]
pub fn text(c: &Context, content: &str) -> Text { c.document.create_text_node(content) }

pub fn button(c: &Context, children: &(impl Mountable + ?Sized), on_click: &Function) -> Element {
	let e = element(c, "button", children);

	e.add_event_listener_with_callback("click", on_click)
		.unwrap();

	e
}

#[must_use]
pub fn text_input(c: &Context) -> HtmlInputElement {
	element(c, "input", &NoOpMountable).unchecked_into::<HtmlInputElement>()
}
