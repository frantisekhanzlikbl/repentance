use repentance_core::facade;
use web_sys::Node;

use crate::Mountable;

// NoOp //

pub struct NoOpMountable;

impl Mountable for NoOpMountable {
	fn mount(&self, _parent: &Node) {}
}

// Node //

facade!(pub MountableNode(Node));

impl Mountable for MountableNode {
	fn mount(&self, parent: &Node) { parent.append_child(self).unwrap(); }
}
