mod attributes;
mod children;
mod display;

use proc_macro2::TokenStream;
use quote::ToTokens;
use syn::{parse::Parse, Ident};

use crate::codegen::element;

#[derive(Debug)]
pub struct Element {
	name: Ident,
	attributes: attributes::Optional,
	children: children::Optional,
}

impl Element {
	pub fn display_xml(&self) -> display::Xml { self.into() }

	pub fn display_simple(&self) -> display::Simple { self.into() }
}

impl Parse for Element {
	fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
		Ok(Self {
			name: input.parse()?,
			attributes: input.parse()?,
			children: input.parse()?,
		})
	}
}

impl ToTokens for Element {
	fn to_tokens(&self, tokens: &mut TokenStream) {
		tokens.extend(Some(element(
			&self.name.to_string(),
			self.children.to_token_stream(),
		)))
	}
}
