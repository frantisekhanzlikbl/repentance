mod display;
mod optional;

use crate::codegen::child_list_dyn;

use super::super::Node;
pub use optional::Optional;
use proc_macro2::TokenStream;
use quote::ToTokens;
use syn::{
	bracketed,
	parse::{Parse, ParseStream},
	punctuated::Punctuated,
	token::Comma,
};

#[derive(Debug)]
pub struct Children {
	pub(crate) items: Punctuated<Node, Comma>,
}

impl Children {
	pub fn display_xml(&self) -> display::Xml { self.into() }

	pub fn display_simple(&self) -> display::Simple { self.into() }
}

impl Parse for Children {
	fn parse(input: ParseStream) -> syn::Result<Self> {
		let content;
		bracketed!(content in input);
		let items = content.parse_terminated(Node::parse)?;

		Ok(Self { items })
	}
}

impl ToTokens for Children {
	fn to_tokens(&self, tokens: &mut TokenStream) {
		tokens.extend(Some(child_list_dyn(
			self.items.iter().map(ToTokens::to_token_stream),
		)))
	}
}
