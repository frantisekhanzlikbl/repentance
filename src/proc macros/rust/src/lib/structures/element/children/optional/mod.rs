mod display;

use proc_macro2::TokenStream;
use quote::ToTokens;
use repentance_core::facade;
use syn::{
	parse::{Parse, ParseStream},
	token::Bracket,
};

use super::Children;

facade!(
	#[derive(Debug)]
	pub Optional(Option<Children>)
);

impl Optional {
	pub fn display_xml(&self) -> display::Xml { self.into() }

	pub fn display_simple(&self) -> display::Simple { self.into() }
}

impl Parse for Optional {
	fn parse(input: ParseStream) -> syn::Result<Self> {
		Ok(if input.peek(Bracket) {
			Some(input.parse()?)
		} else {
			None
		}
		.into())
	}
}

impl ToTokens for Optional {
	fn to_tokens(&self, tokens: &mut TokenStream) {
		tokens.extend(self.as_ref().map(ToTokens::to_token_stream))
	}
}
