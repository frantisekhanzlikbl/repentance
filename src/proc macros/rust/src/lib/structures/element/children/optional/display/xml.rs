use super::super::Optional;

use repentance_core::facade;
use std::fmt::Display;

facade!(
	pub Xml<'a>(&'a Optional)
);

impl Display for Xml<'_> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match &****self {
			Some(inner) => write!(f, "{}", inner.display_xml()),
			None => Ok(()),
		}
	}
}
