mod display;

use std::convert::{TryFrom, TryInto};
use syn::{parse::Parse, Ident, Lit, LitStr};

#[derive(Debug)]
pub enum Name {
	LitStr(LitStr),
	Ident(Ident),
}

impl Name {
	pub fn display_xml(&self) -> display::Xml { self.into() }

	pub fn display_simple(&self) -> display::Simple { self.into() }
}

const UNEXPECTED_TOKEN_MESSAGE: &str =
	"Unexpected token - Only identifiers or string literals allowed.";

impl TryFrom<Lit> for Name {
	type Error = syn::Error;

	fn try_from(value: Lit) -> Result<Self, Self::Error> {
		match value {
			Lit::Str(inner) => Ok(Self::LitStr(inner)),
			rest => Err(syn::Error::new(rest.span(), UNEXPECTED_TOKEN_MESSAGE)),
		}
	}
}

impl Parse for Name {
	fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
		if input.peek(Ident) {
			Ok(Self::Ident(input.parse()?))
		} else if input.peek(Lit) {
			Ok((input.parse(): Result<Lit, _>)?.try_into()?)
		} else {
			Err(syn::Error::new(input.span(), UNEXPECTED_TOKEN_MESSAGE))
		}
	}
}
