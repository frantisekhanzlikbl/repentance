mod display;

use super::Attributes;

use repentance_core::facade;
use syn::{
	parse::{Parse, ParseStream},
	token::Paren,
};

facade!(
	#[derive(Debug)]
	pub Optional(Option<Attributes>)
);

impl Optional {
	pub fn display_xml(&self) -> display::Xml { self.into() }

	pub fn display_simple(&self) -> display::Simple { self.into() }
}

impl Parse for Optional {
	fn parse(input: ParseStream) -> syn::Result<Self> {
		Ok(if input.peek(Paren) {
			Some(input.parse()?)
		} else {
			None
		}
		.into())
	}
}
