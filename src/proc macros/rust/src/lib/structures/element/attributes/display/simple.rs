use crate::structures::element::attributes::Attributes;
use repentance_core::facade;
use std::fmt::Display;

facade!(
	pub Simple<'a>(&'a Attributes)
);

impl Display for Simple<'_> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		self.items
			.iter()
			.try_for_each(|item| writeln!(f, "{},", item.display_simple()))
	}
}
