mod display;
mod name;
mod value;

use name::Name;
use std::convert::TryInto;
use syn::{
	parse::{Parse, ParseStream},
	Lit, Token,
};
use value::Value;

#[derive(Debug)]
pub struct Attribute {
	name: Name,
	value: Value,
}

impl Attribute {
	pub fn display_xml(&self) -> display::Xml { self.into() }

	pub fn display_simple(&self) -> display::Simple { self.into() }
}

impl Parse for Attribute {
	fn parse(input: ParseStream) -> syn::Result<Self> {
		let name = input.parse()?;
		input.parse::<Token!(=)>()?;
		let value = input.parse::<Lit>()?.try_into()?;

		Ok(Self { name, value })
	}
}
