mod display;

use proc_macro2::TokenStream;
use quote::ToTokens;
use std::convert::{TryFrom, TryInto};
use syn::{
	parse::{Parse, ParseStream},
	LitStr,
};

use crate::codegen::text;

#[derive(Debug)]
pub enum Literal {
	Str(LitStr),
}

impl Literal {
	pub fn display_xml(&self) -> display::Xml { self.into() }

	pub fn display_simple(&self) -> display::Simple { self.into() }
}

impl TryFrom<syn::Lit> for Literal {
	type Error = syn::Error;

	fn try_from(value: syn::Lit) -> Result<Self, Self::Error> {
		match value {
			syn::Lit::Str(inner) => Ok(Self::Str(inner)),
			rest => Err(syn::Error::new(
				rest.span(),
				"Only string, number and boolean literals are allowed here",
			)),
		}
	}
}

impl Parse for Literal {
	fn parse(input: ParseStream) -> syn::Result<Self> { Ok(input.parse::<syn::Lit>()?.try_into()?) }
}

impl ToTokens for Literal {
	fn to_tokens(&self, tokens: &mut TokenStream) {
		match self {
			Self::Str(inner) => tokens.extend(Some(text(&inner.value()))),
		}
	}
}
